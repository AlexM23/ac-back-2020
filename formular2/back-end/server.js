const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end",
});

var emailsArray = [];

connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS abonamente_metrou(nume VARCHAR(255), prenume  VARCHAR(255), cnp VARCHAR(20), varsta VARCHAR(3), email  VARCHAR(255) , telefon  VARCHAR(255),data_inceput  VARCHAR(10),data_sfarsit  VARCHAR(10), gen VARCHAR(1))"; //intre paranteze campurile cu datele
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });

  const requireEmailsSQL = 
    "SELECT email FROM abonamente_metrou";
  connection.query(requireEmailsSQL, function(err, result){
    if (err) throw err;
    result.forEach(r => {
      emailsArray.push(r.email);
    })
    console.log(emailsArray);
  });
});

function date_corecte(inceput, sfarsit){

  //functie pentru a verifica data inceput<sfarsit ca data

  var dayI = inceput[0] + inceput[1];
  dayI = parseInt(dayI);
  var dayS = sfarsit[0] + sfarsit[1];
  dayS = parseInt(dayS);
  var monthI = inceput[3] + inceput[4];
  monthI = parseInt(monthI);
  var monthS = sfarsit[3] + sfarsit[4];
  monthS = parseInt(monthS);
  var yearI = inceput[6] + inceput[7] + inceput[8] + inceput[9];
  yearI = parseInt(yearI);
  var yearS = sfarsit[6] + sfarsit[7] + sfarsit[8] + sfarsit[9];
  yearS = parseInt(yearS);
  
  if (yearI < yearS)
    return true;
  else if (yearI === yearS){
    if (monthI < monthS)
      return true;
    else if (monthI === monthS){
      if (dayI < dayS)
        return true;
      else return false;
    }
    else return false;
  }
  else return false;

}

app.post("/bilet", (req, res) => {
  let bilet = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    cnp: req.body.cnp,
    email: req.body.email,
    data_inceput: req.body.data_inceput,
    data_sfarsit: req.body.data_sfarsit,
    varsta: req.body.varsta,
  };
  let error = [];
 
  if (!bilet.nume || !bilet.prenume || !bilet.telefon 
    || !bilet.telefon || !bilet.email || !bilet.data_inceput
    || !bilet.data_sfarsit || !bilet.cnp || !bilet.varsta){
      error.push("Unul sau mai multe campuri nu au fost completate!");
      console.log("Unul sau mai multe campuri nu au fost completate");
    }
    else {
    
      if (bilet.nume.length < 3 || bilet.nume.length > 30){
        error.push("Nume invalid!");
        console.log("Nume invalid");
      } else if (!bilet.nume.match("^[A-Za-z]+$")){
        error.push("Numele trebuie sa contina doar litere!");
        console.log("Numele trebuie sa contina doar litere");
      }

      if (bilet.prenume.length < 3 || bilet.prenume.length > 30){
        error.push("Nume invalid!");
        console.log("Nume invalid");
      } else if (!bilet.prenume.match("^[A-Za-z]+$")){
        error.push("Numele trebuie sa contina doar litere!");
        console.log("Numele trebuie sa contina doar litere");
      }

      if (bilet.cnp.length !== 13){
        error.push("CNP-ul nu este valid!");
        console.log("CNP-ul nu este valid!");
      } else if (!bilet.cnp.match("^[0-9]+$")){
        error.push("CNP-ul trebuie sa contina doar cifre!");
        console.log("CNP-ul trebuie sa contina doar cifre");
      }
      
      if (bilet.telefon.length !== 10){
        error.push("Telefon invalid!");
        console.log("Telefon invalid");
      } else if (!bilet.telefon.match("^[0-9]+$")){
        error.push("Telefonul trebuie sa contina doar cifre!");
        console.log("Telefonul trebuie sa contina doar cifre");
      }

      var ok = true;
      emailsArray.forEach(e => {
        if (e === bilet.email)
          ok = false;
      });
      if (!ok){
        error.push("Email-ul introdus este deja inregistrat!");
        console.log("Email deja inregistrat");
      }

      if (!bilet.email.includes("@") && !bilet.email.includes(".com")){
        error.push("Email invalid!");
        console.log("Email invalid");
      }

      if (!bilet.data_inceput.match("^[0-9/]+$")){
        error.push("Data de inceput contine altceva decat cifre si slash!");
        console.log("Data inceput scris gresit");
      } else {
        if (bilet.data_inceput[2] !== '/' || bilet.data_inceput[5] !== '/'){
          error.push("Data de inceput nu este valida!")
          console.log("Data inceput nu este valida");
        }
      }

      if (!bilet.data_sfarsit.match("^[0-9/]+$")){
        error.push("Data de sfarsit contine altceva decat cifre si slash!");
        console.log("Data sfarsit scris gresit");
      } else {
        if (bilet.data_sfarsit[2] !== '/' || bilet.data_sfarsit[5] !== '/'){
          error.push("Data de sfarsit nu este valida!")
          console.log("Data sfarsit nu este valida");
        }
      }

      if (!date_corecte(bilet.data_inceput, bilet.data_sfarsit)){
        error.push("Data de sfarsit este inaintea datei de inceput!");
        console.log("Datele nu sunt corecte");
      }

      var today = new Date();
      today = parseInt(today.getDate()) + '/' + parseInt(today.getMonth() + 1) + '/' + today.getFullYear();

      if (!date_corecte(bilet.data_inceput, today) || !date_corecte(today, bilet.data_sfarsit)){
        console.log("Data de astazi nu este incadrata intre datile inscrise");
        error.push("Data de astazi nu este incadrata intre datile inscrise!");
      }

      if (bilet.varsta.length < 1 || bilet.varsta.length > 3){
        error.push("Varsta invalida!");
        console.log("Varsta invalida!");
      } else if (!bilet.varsta.match("^[0-9]+$")){
        error.push("Varsta trebuie sa contina doar cifre!");
        console.log("Varsta trebuie sa contina doar cifre");
      }

    }

  var gen = parseInt(bilet.cnp[0]) % 2 == 1 ? 'M' : 'F';

  if (error.length === 0) {

    const sql = `INSERT INTO abonamente_metrou (nume,
      prenume,
      cnp,
      varsta,
      email,
      telefon,
      data_inceput,
      data_sfarsit,
      gen) VALUES (?,?,?,?,?,?,?,?,?)`;
    connection.query(
      sql,
      [
        bilet.nume,
        bilet.prenume,
        bilet.cnp,
        bilet.varsta,
        bilet.email,
        bilet.telefon,
        bilet.data_inceput,
        bilet.data_sfarsit,
        gen
      ],
      function (err, result) {
        if (err) throw err;
        console.log("Abonament realizat cu succes!");
        res.status(200).send({
          message: "Abonament realizat cu succes",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("Abonamentul nu a putut fi creat!");
  }
  app.use('/', express.static('../front-end'))
});